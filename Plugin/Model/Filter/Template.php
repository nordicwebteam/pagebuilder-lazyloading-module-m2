<?php

declare(strict_types=1);

namespace NWT\PageBuilderLazyLoading\Plugin\Model\Filter;

use DOMDocument;
use DOMException;
use DOMXPath;
use Exception;
use Magento\Framework\Math\Random;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\ConfigInterface;
use Magento\PageBuilder\Plugin\Filter\TemplatePlugin;
use Psr\Log\LoggerInterface;

/**
 * Specific template filters for Page Builder content. Adds lazyload class for elements with background images.
 */
class Template
{
    /**
     * @var ConfigInterface
     */
    private $viewConfig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DOMDocument
     */
    private $domDocument;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var array
     */
    private $scripts;

    /**
     * @param LoggerInterface $logger
     * @param ConfigInterface $viewConfig
     * @param Random $mathRandom
     * @param Json $json
     */
    public function __construct(
        LoggerInterface $logger,
        ConfigInterface $viewConfig,
        Random $mathRandom,
        Json $json
    ) {
        $this->logger = $logger;
        $this->viewConfig = $viewConfig;
        $this->mathRandom = $mathRandom;
        $this->json = $json;
    }

    public function afterfilter(\Magento\PageBuilder\Model\Filter\Template $subject, $result) : string
    {
        // Validate if the filtered result requires background image processing
        // Adds lazyload class if the element has a background image
        if (preg_match(TemplatePlugin::BACKGROUND_IMAGE_PATTERN, $result)) {
            $this->domDocument = false;
            $this->scripts = [];
            $document = $this->getDomDocument($result);
            $this->generateBackgroundImageStylesLazyload($document);

            // If a document was retrieved we've modified the output so need to retrieve it from within the document
            if (isset($document)) {
                // Match the contents of the body from our generated document
                preg_match(
                    '/<body>(.+)<\/body><\/html>$/si',
                    $document->saveHTML(),
                    $matches
                );

                if (!empty($matches)) {
                    $docHtml = $matches[1];

                    // restore any encoded directives
                    $docHtml = preg_replace_callback(
                        '/=\"(%7B%7B[^"]*%7D%7D)\"/m',
                        function ($matches) {
                            return urldecode($matches[0]);
                        },
                        $docHtml
                    );

                    if (isset($uniqueNodeNameToDecodedOuterHtmlMap)) {
                        foreach ($uniqueNodeNameToDecodedOuterHtmlMap as $uniqueNodeName => $decodedOuterHtml) {
                            $docHtml = str_replace(
                                '<' . $uniqueNodeName . '>' . '</' . $uniqueNodeName . '>',
                                $decodedOuterHtml,
                                $docHtml
                            );
                        }
                    }

                    $result = $docHtml;
                }

                $result = $this->unmaskScriptTags($result);
            }
        }

        return $result;
    }

    /**
     * Create a DOM document from a given string
     *
     * @param string $html
     *
     * @return DOMDocument
     */
    private function getDomDocument(string $html) : DOMDocument
    {
        $this->domDocument = $this->createDomDocument($html);

        return $this->domDocument;
    }

    /**
     * Create a DOMDocument from a string
     *
     * @param string $html
     *
     * @return DOMDocument
     */
    private function createDomDocument(string $html) : DOMDocument
    {
        $html = $this->maskScriptTags($html);

        $domDocument = new DOMDocument('1.0', 'UTF-8');
        set_error_handler(
            function ($errorNumber, $errorString) {
                throw new DOMException($errorString, $errorNumber);
            }
        );
        $string = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        try {
            libxml_use_internal_errors(true);
            $domDocument->loadHTML(
                '<html><body>' . $string . '</body></html>',
                LIBXML_SCHEMA_CREATE
            );
            libxml_clear_errors();
        } catch (Exception $e) {
            restore_error_handler();
            $this->logger->critical($e);
        }
        restore_error_handler();

        return $domDocument;
    }

    /**
     * Masks "x-magento-template" script tags in html content before loading it into DOM parser
     *
     * DOMDocument::loadHTML() will remove any closing tag inside script tag and will result in broken html template
     *
     * @param string $content
     * @return string
     * @see https://bugs.php.net/bug.php?id=52012
     */
    private function maskScriptTags(string $content): string
    {
        $tag = 'script';
        $content = preg_replace_callback(
            sprintf('#<%1$s[^>]*type="text/x-magento-template\"[^>]*>.*?</%1$s>#is', $tag),
            function ($matches) {
                $key = $this->mathRandom->getRandomString(32, $this->mathRandom::CHARS_LOWERS);
                $this->scripts[$key] = $matches[0];
                return '<' . $key . '>' . '</' . $key . '>';
            },
            $content
        );
        return $content;
    }

    private function generateCssLazyload(string $elementClass, array $images) : string
    {
        $css = [];
        if (isset($images['desktop_image'])) {
            $css['.' . $elementClass . '.lazyload'] = [
                'background-image' => 'none',
            ];
        }
        if (isset($images['mobile_image']) && $this->getMediaQuery('mobile')) {
            $css[$this->getMediaQuery('mobile')]['.' . $elementClass . '.lazyload'] = [
                'background-image' => 'none',
            ];
        }
        if (isset($images['mobile_image']) && $this->getMediaQuery('mobile-small')) {
            $css[$this->getMediaQuery('mobile-small')]['.' . $elementClass . '.lazyload'] = [
                'background-image' => 'none',
            ];
        }
        return $this->cssFromArray($css);
    }

    private function generateBackgroundImageStylesLazyload(DOMDocument $document) : void
    {
        $xpath = new DOMXPath($document);
        $nodes = $xpath->query('//*[@data-background-images]');
        foreach ($nodes as $node) {
            /* @var DOMElement $node */
            $backgroundImages = $node->attributes->getNamedItem('data-background-images');
            if ($backgroundImages->nodeValue !== '') {
                $currentClass = $node->getAttribute('class');
                if (str_contains($currentClass, 'background-image-')) {
                    $elementClass = substr($currentClass, strpos($currentClass, "background-image-"));
                    $elementClass = strtok($elementClass, ' ');
                } else {
                    $elementClass = uniqid('background-image-');
                }
                // phpcs:ignore Magento2.Functions.DiscouragedFunction
                $images = $this->json->unserialize(stripslashes($backgroundImages->nodeValue));
                if (count($images) > 0) {
                    $style = $xpath->document->createElement(
                        'style',
                        $this->generateCssLazyload($elementClass, $images)
                    );

                    $styleChild = null;
                    foreach ($node->parentNode->childNodes as $child) {
                        if ($child->tagName == 'style') {
                            $styleChild = $child;
                        }
                    }
                    $style->setAttribute('type', 'text/css');
                    $node->parentNode->insertBefore($style, $styleChild);

                    // Append our new class to the DOM element
                    if ($node->attributes->getNamedItem('class')) {
                        $classes = $node->attributes->getNamedItem('class')->nodeValue . ' lazyload ';
                        $node->setAttribute('class', $classes);
                    }
                }
            }
        }
    }
    
    /**
     * Generate the mobile media query from view configuration
     *
     * @param string $view
     * @return null|string
     */
    private function getMediaQuery(string $view) : ?string
    {
        $breakpoints = $this->viewConfig->getViewConfig()->getVarValue(
            'Magento_PageBuilder',
            'breakpoints/' . $view . '/conditions'
        );
        if ($breakpoints && count($breakpoints) > 0) {
            $mobileBreakpoint = '@media only screen ';
            foreach ($breakpoints as $key => $value) {
                $mobileBreakpoint .= 'and (' . $key . ': ' . $value . ') ';
            }
            return rtrim($mobileBreakpoint);
        }
        return null;
    }

    /**
     * Generate a CSS string from an array
     *
     * @param array $css
     *
     * @return string
     */
    private function cssFromArray(array $css) : string
    {
        $output = '';
        foreach ($css as $selector => $body) {
            if (is_array($body)) {
                $output .= $selector . ' {';
                $output .= $this->cssFromArray($body);
                $output .= '}';
            } else {
                $output .= $selector . ': ' . $body . ';';
            }
        }
        return $output;
    }

    /**
     * Replaces masked "x-magento-template" script tags with their corresponding content
     *
     * @param string $content
     * @return string
     * @see maskScriptTags()
     */
    private function unmaskScriptTags(string $content): string
    {
        foreach ($this->scripts as $key => $script) {
            $content = str_replace(
                '<' . $key . '>' . '</' . $key . '>',
                $script,
                $content
            );
        }
        return $content;
    }
}
